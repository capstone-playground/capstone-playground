package com.example.capstonetest.hello;

import com.example.capstonetest.fxml.FXMLModule;
import com.example.capstonetest.fxmlcommunicator.FXMLCommModule;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class HelloModule extends AbstractModule {
    @Override
    protected void configure() {
//        install(new FXMLCommModule());

        bind(HelloService.class).to(HelloServiceImpl.class).in(Singleton.class);
    }
}
