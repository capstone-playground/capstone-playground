package com.example.capstonetest.hello;

import com.example.capstonetest.fxml.FXMLService;
import com.example.capstonetest.hi.HiController;
import com.google.inject.Inject;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class HelloController {
    @Inject
    private HelloService helloService;

    @Inject
    private FXMLService fxmlCommService;

    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        this.welcomeText.setText(this.helloService.getHelloMessage());
    }

    @FXML
    protected void onHiButtonClick(ActionEvent event) {
        var node = (Node) event.getSource();
        var stage = (Stage) node.getScene().getWindow();
        this.fxmlCommService.switchSceneRoot(stage, HiController.class, "hi-view.fxml");

        event.consume();
    }
}