package com.example.capstonetest.hello;

public class HelloServiceImpl implements HelloService {
    @Override
    public String getHelloMessage() {
        System.out.println(this.getClass().getName() + ": Hello from the other side");
        return "Hello world from JavaFX!";
    }
}
