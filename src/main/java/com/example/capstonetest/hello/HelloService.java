package com.example.capstonetest.hello;

public interface HelloService {
    public String getHelloMessage();
}
