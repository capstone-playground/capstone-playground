package com.example.capstonetest.fxmlcommunicator;

import com.example.capstonetest.fxml.FXMLModule;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

public class FXMLCommModule extends AbstractModule {
    @Override
    public void configure() {
        install(new FXMLModule());

        bind(FXMLCommService.class).in(Singleton.class);
    }
}
