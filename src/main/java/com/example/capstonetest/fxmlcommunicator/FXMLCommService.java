package com.example.capstonetest.fxmlcommunicator;

import com.example.capstonetest.fxml.FXMLLoaderProvider;
import com.google.inject.Inject;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.stage.Stage;

import java.io.IOException;

//@Singleton
public class FXMLCommService {
    private final FXMLLoaderProvider fxmlLoaderProvider;

    @Inject
    public FXMLCommService(FXMLLoaderProvider fxmlLoaderProvider) {
        this.fxmlLoaderProvider = fxmlLoaderProvider;
    }

    public Node createFxNode(Class<?> clazz, String resourcePath) {
        var path = clazz.getResource(resourcePath);
        try {
            var loader = this.fxmlLoaderProvider.get();
            loader.setLocation(path);
            return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void switchSceneRoot(Stage stage, Class<?> clazz, String resourcePath) {
        Parent parent = (Parent) this.createFxNode(clazz, resourcePath);
        stage.getScene().setRoot(parent);
    }
}
