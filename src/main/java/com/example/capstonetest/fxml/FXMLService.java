package com.example.capstonetest.fxml;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

//@Singleton
public class FXMLService {
    private final FXMLLoaderProvider fxmlLoaderProvider;

    @Inject
    public FXMLService(FXMLLoaderProvider fxmlLoaderProvider) {
        this.fxmlLoaderProvider = fxmlLoaderProvider;
    }

    public Node createFxNode(Class<?> clazz, String resourcePath) {
        var path = clazz.getResource(resourcePath);
        try {
            var loader = this.fxmlLoaderProvider.get();
            loader.setLocation(path);
            return loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void switchSceneRoot(Stage stage, Class<?> clazz, String resourcePath) {
        Parent parent = (Parent) this.createFxNode(clazz, resourcePath);
        stage.getScene().setRoot(parent);
    }
}
