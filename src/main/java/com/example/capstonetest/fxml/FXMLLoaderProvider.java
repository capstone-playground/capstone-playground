package com.example.capstonetest.fxml;

import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Provider;
import javafx.fxml.FXMLLoader;

public class FXMLLoaderProvider implements Provider<FXMLLoader> {
    @Inject
    Injector injector;

    @Override
    public FXMLLoader get() {
        var loader = new FXMLLoader();
        loader.setControllerFactory(this.injector::getInstance);
        return loader;
    }
}
