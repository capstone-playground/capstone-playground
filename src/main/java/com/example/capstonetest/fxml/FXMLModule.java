package com.example.capstonetest.fxml;

import com.example.capstonetest.hello.HelloModule;
import com.example.capstonetest.hello.HelloService;
import com.example.capstonetest.hello.HelloServiceImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Singleton;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;

public class FXMLModule extends AbstractModule {
    @Override
    protected void configure() {
        install(new HelloModule());
//        this.

        bind(FXMLLoader.class).toProvider(FXMLLoaderProvider.class);
        bind(FXMLService.class).in(Singleton.class);

//        bind(HelloService.class).to(HelloServiceImpl.class);
//        bind(HiServices.class).to(HiServiceImpl.class);
    }
}
