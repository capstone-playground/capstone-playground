package com.example.capstonetest.fxml;

public interface ControllerProps<T> {
    public void setData(T props);
}
