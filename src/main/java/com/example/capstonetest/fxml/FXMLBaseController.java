package com.example.capstonetest.fxml;

import java.net.URL;

public abstract class FXMLBaseController {
    public abstract URL getFxmlPath();
}
