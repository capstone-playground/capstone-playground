package com.example.capstonetest;

import com.example.capstonetest.hello.HelloController;
import com.google.inject.Guice;
import com.google.inject.Injector;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        try {
            Injector injector = Guice.createInjector(new AppModule());
            FXMLLoader fxmlLoader = injector.getInstance(FXMLLoader.class);

            var fxmlInputStream = HelloController.class.getResourceAsStream("hello-view.fxml");
            Parent parent = fxmlLoader.load(fxmlInputStream);
            stage.setScene(new Scene(parent));
            stage.setTitle("Hello!");
            stage.setOnCloseRequest(event -> {
                System.exit(0);
            });
            stage.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}