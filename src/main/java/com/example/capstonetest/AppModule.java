package com.example.capstonetest;

import com.example.capstonetest.fxml.FXMLLoaderProvider;
import com.example.capstonetest.fxml.FXMLModule;
import com.example.capstonetest.fxmlcommunicator.FXMLCommModule;
import com.example.capstonetest.hello.HelloModule;
import com.google.inject.AbstractModule;
import javafx.fxml.FXMLLoader;

public class AppModule extends AbstractModule {
    @Override
    protected void configure() {
//        install(new HelloModule());
//
//        install(new FXMLModule());
//
        install(new FXMLCommModule());
    }
}
