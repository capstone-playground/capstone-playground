package com.example.capstonetest.hi;

import com.example.capstonetest.fxml.FXMLService;
import com.example.capstonetest.hello.HelloController;
import com.google.inject.Inject;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.stage.Stage;

public class HiController {
    @Inject
    private FXMLService fxmlService;

    @FXML
    public void onHelloBtnClick(ActionEvent event) {
        var node = (Node) event.getSource();
        var stage = (Stage) node.getScene().getWindow();
        this.fxmlService.switchSceneRoot(stage, HelloController.class, "hello-view.fxml");

        event.consume();
    }
}
