package com.example.capstonetest.hi;

import com.example.capstonetest.fxml.FXMLService;
import com.google.inject.AbstractModule;

public class HiModule extends AbstractModule {
    private final FXMLService fxmlService;

    public HiModule(FXMLService fxmlService) {
        this.fxmlService = fxmlService;
    }
}
