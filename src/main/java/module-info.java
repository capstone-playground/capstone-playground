module com.example.capstonetest {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.google.guice;

    opens com.example.capstonetest to javafx.fxml, com.google.guice;
    exports com.example.capstonetest;
    opens com.example.capstonetest.hello to javafx.fxml, com.google.guice;
    exports com.example.capstonetest.hello;
    opens com.example.capstonetest.hi to javafx.fxml, com.google.guice;
    exports com.example.capstonetest.hi;
    opens com.example.capstonetest.fxml to com.google.guice;
    exports com.example.capstonetest.fxml;
    opens com.example.capstonetest.fxmlcommunicator to com.google.guice;
    exports com.example.capstonetest.fxmlcommunicator;
}